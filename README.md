# Macro Keyboard keycodes

Keycode definitions for the Macro-Keyboard and Via.  
Generate the code files for both projects to ensure compatibility between both projects/


## Usage

- Create environment

```shell
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
```

- Run generator

```shell
python gen.py
```

## Note about versions

Definition version follow Via protocol version with a major increase.

| Via Version  | Our Version | Hex Version | Filename Example    |
|:------------:|:-----------:|:-----------:|:--------------------|
| v11 Original |    v0.11    |   0x000B    | keycodes_v0.11.toml |
| v11 Custom   |    v1.11    |   0x010B    | keycodes_v1.11.toml |

Note: Via/QMK only versions the protocol version (aka supported commands) and not the keycodes defined.
We hack this versionning scheme to introduce our own custom commands (aka Macros) + keycodes by increasing the major version but trying to follow the original VIA version as close as possible.