import logging
import os
import pathlib
import subprocess
from collections.abc import Mapping

import toml
import yaml

try:
    import ujson as json
except ImportError:
    import json

try:
    FileNotFoundError
except NameError:
    FileNotFoundError = OSError

logger = logging.getLogger(__name__)


SUPPORTED_EXTENSIONS = ["json", "toml", "yaml", "yml"]


class ConfigParserError(Exception):
    """Denotes failing to parse configuration file."""

    def __init__(self, message, *args):
        self.message = message
        super(ConfigParserError, self).__init__(message, *args)

    def __str__(self):
        return "While parsing config: {0}".format(self.message)


def abs_pathify(in_path):
    logger.info("Trying to resolve absolute path to {0}".format(in_path))

    try:
        return pathlib.Path(in_path).resolve()
    except FileNotFoundError as e:
        logger.error('Couldn"t discover absolute path: {0}'.format(e))
        return ""


def exists(path):
    try:
        os.stat(str(path))
        return True
    except FileNotFoundError:
        return False


def unmarshall_config_reader(r, d, config_type):
    config_type = config_type.lower()

    if config_type in ["yaml", "yml"]:
        try:
            f = yaml.safe_load(r)
            try:
                d.update(yaml.safe_load(f))
            except AttributeError:  # to read files
                d.update(f)
        except Exception as e:
            raise ConfigParserError(e)

    elif config_type == "json":
        try:
            f = json.loads(r)
            d.update(f)
        except Exception as e:
            raise ConfigParserError(e)

    elif config_type == "toml":
        try:
            try:
                d.update(toml.loads(r))
            except TypeError:  # to read files
                try:
                    d.update(toml.load(r))
                except TypeError:  # to read streams
                    d.update(r)
        except Exception as e:
            raise ConfigParserError(e)

    return d


def file_writer(path, lines):

    pathlib.Path(os.path.dirname(path)).mkdir(parents=True, exist_ok=True)

    logger.info("Writing to file: {}".format(path))
    file = open(path, "w")
    for line in lines:
        file.write(line + "\n")
    file.close()


def merge_dict(dict_1, dict_2):
    # Perform recursive merge of two dictionnaries
    for key, value in dict_2.items():
        if isinstance(value, Mapping):
            dict_1[key] = merge_dict(dict_1.get(key, {}), value)
        else:
            dict_1[key] = value

    return dict_1


def git_commit_hash():
    return subprocess.check_output(['git', 'rev-parse', 'HEAD']).decode('ascii').strip()

def git_commit_short_hash():
    return subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD']).decode('ascii').strip()

def git_describe():
    return subprocess.check_output(['git', 'describe', '--tags', '--always', '--dirty']).decode('ascii').strip()
