import argparse
import datetime
import logging
import json
import os
from collections import Counter

import utils
from settings import settings, setup_vyper
from generators import *

SUPPORTED_LANGUAGES = ["c", "ts"]

logger = logging.getLogger("gen")


def read_definition(file_path, file_type):
    logger.debug("Attempting to read file: {}".format(file_path))

    with open(file_path) as fp:
        f = fp.read()

    definition = utils.unmarshall_config_reader(f, {}, file_type)
    return definition


def find_definition(directory, file_type, version):
    logger.debug("Finding definition files in {}".format(directory))

    if file_type not in utils.SUPPORTED_EXTENSIONS:
        raise Exception("Unsupported file extension")

    files = []

    base_file = "keycodes_{}.{}".format(version, file_type)
    sub_file = "keycodes_{}_*.{}".format(version, file_type)

    dir_path = utils.abs_pathify(directory)
    base_file_path = os.path.join(dir_path, base_file)
    if utils.exists(base_file_path):
        logger.info("Found file: {}".format(base_file_path))
        files.append({
            "name": os.path.basename(base_file_path),
            "path": base_file_path,
        })

        for file in dir_path.glob(sub_file):
            logger.info("Found subfile: {}".format(file))
            files.append({
                "name": os.path.basename(file),
                "path": file,
            })

    return files


# Helper function for ranges addresses
def range_overlapping(x, y):
    x_start = x["address"]
    x_stop = x["address"] + x["size"]
    y_start = y["address"]
    y_stop = y["address"] + y["size"]

    if x_start == x_stop or y_start == y_stop:
        return False
    return x_start <= y_stop and y_start <= x_stop


def validate_definition(files):

    logger.info("Validation definitions")

    ranges = {}
    keycodes = {}
    macros = {}
    fail = False
    for file in files:
        # Ranges
        logger.debug("Validating ranges for {}".format(file["name"]))

        # Checking key duplicates
        definition = file.get("definition")
        file_ranges = definition.get("ranges", {})
        set_1 = set(ranges)
        set_2 = set(file_ranges)
        for key in set_1.intersection(set_2):
            logger.error("Duplicate range key '{}' in file {}".format(key, file["name"]))
            fail = True

        ranges.update(file_ranges)

        # Checking name duplicates
        names = [r["name"] for r in ranges.values()]
        duplicates = [k for k,v in Counter(names).items() if v>1]
        for duplicate in duplicates:
            logger.error("Duplicate range '{}' in file {}".format(duplicate, file["name"]))
            fail = True

        # Checking address and size overlaps
        ranges_list = sorted(ranges.values(), key=lambda r: r["address"])

        for i, r in enumerate(ranges_list):
            if (i+1 < len(ranges_list)):
                next_r = ranges_list[i + 1]
                if range_overlapping(r, next_r):
                    logger.error("Overlapping ranges '{}' (0x{:04X}, 0x{:04X}) and '{}' (0x{:04X}, 0x{:04X})".format(
                        r["name"], r["address"], r["address"] + r["size"],
                        next_r["name"], next_r["address"], next_r["address"] + next_r["size"],
                    ))
                    fail = True

        # Keycodes
        logger.debug("Validating keycodes for {}".format(file["name"]))

        # Checking keycode duplicates
        file_keycodes = definition.get("keycodes", {})
        set_1 = set(keycodes)
        set_2 = set(file_keycodes)
        for keycode in set_1.intersection(set_2):
            logger.error("Duplicate keycode '{}' in file {}".format(keycode, file["name"]))
            fail = True

        keycodes.update(file_keycodes)

        # Checking key name and aliases duplicates
        keys = [r["key"] for r in keycodes.values()]
        aliases = [alias for r in keycodes.values() for alias in r.get("aliases", [])]
        keys = keys + aliases
        duplicates = [k for k,v in Counter(keys).items() if v>1]
        for duplicate in duplicates:
            logger.error("Duplicate key '{}' in file {}".format(duplicate, file["name"]))
            fail = True

        # Macros
        logger.debug("Validating macros for {}".format(file["name"]))

        # Checking key duplicates
        definition = file.get("definition")
        file_macros = definition.get("macros", {})
        set_1 = set(macros)
        set_2 = set(file_macros)
        for key in set_1.intersection(set_2):
            logger.error("Duplicate macro key '{}' in file {}".format(key, file["name"]))
            fail = True

        macros.update(file_macros)

        # Checking macro name and aliases duplicates
        macro_list = [r["name"] for r in macros.values()]
        aliases = [alias for r in macros.values() for alias in r.get("aliases", [])]
        macro_list = macro_list + aliases
        duplicates = [k for k,v in Counter(macro_list).items() if v>1]
        for duplicate in duplicates:
            logger.error("Duplicate macro '{}' in file {}".format(duplicate, file["name"]))
            fail = True

    if fail:
        raise Exception("Validation failed")

    return True


def merge_definition(files):
    logger.debug("Merging definitions")

    definition = {}
    for file in files:
        utils.merge_dict(definition, file.get("definition"))

    return definition


def print_definition(definition, key=None):

    d = definition
    if key:
        d = definition[key]
    logger.debug("Loaded definition: \n{}".format(json.dumps(d, indent=4, sort_keys=False)))




def main():
    parser = argparse.ArgumentParser(description="Generate keycode definition files for the Macro-Keyboard")
    i = parser.add_argument_group("Input", "Input configuration")
    i.add_argument("--dir", "-d", type=str, default="definitions", help="The directory containing the definitions (default '%(default)s')")
    i.add_argument("--file-type", "-t", type=str, default="toml", help="The definitions file type (json|hjson|toml|yaml) (default %(default)s)")
    i.add_argument("--version", "-v", type=str, default="dev", help="The definition version to generate (default %(default)s)")
    o = parser.add_argument_group("Output", "Output configuration")
    o.add_argument("--language", "-l", type=str, default="c", help="The language to generate the files to ('c'|'ts') (default '%(default)s')")
    o.add_argument("--out-dir", "-o", type=str, default="out", help="The directory to write the files to (default %(default)s)")
    o.add_argument("--file", "-f", type=str, default="keycodes", help="The name of the file to generate (without extension) (default %(default)s)")
    o.add_argument("--dry-run", action="store_true", default=False, help="Do not write file (only print to cli) (default %(default)s)")
    o.add_argument("--no-print", action="store_true", default=False, help="Do not write output to log (default %(default)s)")
    l = parser.add_argument_group("Logs")
    l.add_argument("--log-format", type=str, default="[%(asctime)s] [%(levelname)7s] %(name)10s:%(lineno)-3d - %(funcName)-20s - %(message)s", help="Log format (default %(default)s)")
    l.add_argument("--log-level", type=str, default="INFO", help="Log level (default %(default)s)")
    c = parser.add_argument_group("Config", "Config files settings")
    c.add_argument("--config-file", "-c", type=str, default="config.dev", help="Config file to use (default '%(default)s')")
    c.add_argument("--config-dir", type=str, default="configs", help="Config files directory (default '%(default)s')")
    c.add_argument("--config-type", type=str, default="toml", help="Config files type (default '%(default)s')")
    c.add_argument("--environment_variables_prefix", type=str, default="app", help="Prefix for environment variables (default '%(default)s')")

    setup_vyper(parser)

    logging.basicConfig(level=settings.get("log_level"), format=settings.get("log_format"))
    logger.info("Hello")

    if settings.get("language") not in SUPPORTED_LANGUAGES:
        raise Exception("Unsupported generation language, needs to one of {}".format(SUPPORTED_LANGUAGES))

    files = find_definition(settings.get("dir"), settings.get("file_type"), settings.get("version"))
    logger.debug(files)
    for file in files:
        definition = read_definition(file["path"], settings.get("file_type"))
        file.update({
            "definition": definition
        })
    validate_definition(files)
    definition = merge_definition(files)
    # print_definition(definition)

    # Add version in definition
    if "version" not in definition:
        definition["version"] = settings.get("version")

    if (settings.get("language") == "c"):
        generator = GeneratorC(settings.get("out_dir"), settings.get("file"))
        generator.generate_file(definition)
        if not settings.get("no_print"):
            generator.print_file()
    elif (settings.get("language") == "ts"):
        generator = GeneratorTS(settings.get("out_dir"), settings.get("file"))
        generator.generate_file(definition)
        if not settings.get("no_print"):
            generator.print_file()

    if not settings.get("dry_run"):
        generator.write_file()

    logger.info("Bye")







if __name__ == "__main__":
    main()