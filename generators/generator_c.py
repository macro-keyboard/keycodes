import logging
import datetime

import utils


class GeneratorC:

    LANGUAGE = "c"
    
    def __init__(self, directory, file):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.directory = directory
        self.file = file

        self.lines = []

        self.logger.debug("Init C file generator")


    def generate_file(self, definition):
        self.logger.info("Generating header file content")

        self.lines.append("#ifndef KEY_DEFINITIONS_H")
        self.lines.append("#define KEY_DEFINITIONS_H\n")
        self.lines.append("/*")
        self.lines.append("File is generated using the keycodes definition json or toml files")
        self.lines.append("  Generated on: {:%d-%m-%Y %H:%M:%S}".format(datetime.datetime.now()))
        self.lines.append("  Version: {}".format(definition["version"]))
        self.lines.append("  Git commit (keycodes repository): {}".format(utils.git_describe()))
        self.lines.append("*/")

        self.generate_ranges(definition)
        self.generate_keycodes(definition)
        self.generate_macros(definition)

        self.lines.append("#endif")


    def generate_ranges(self, definition):
        self.logger.debug("Generating ranges")
    
        self.lines.append("\nenum keycode_ranges {")

        for name, r in definition.get("ranges").items():
            self.lines.append("\t{:<30} = 0x{:04X},".format(r["name"], r["address"]))
            self.lines.append("\t{:<30} = 0x{:04X},".format(r["name"]+"_MAX", r["address"]+r["size"]))

        self.lines.append("};\n")

        self.lines.append("// Switch statement helpers")
        for name, r in definition.get("ranges").items():
            self.lines.append("#define {:<26} {} ... {}".format(r["name"]+"_RANGE", r["name"], r["name"]+"_MAX"))

        self.lines.append("\n")


    def generate_keycodes(self, definition):
        self.logger.debug("Generating keycodes")

        keycodes = dict(sorted(definition.get("keycodes", {}).items()))

        self.lines.append("\nenum keycodes {")
        self.lines.append("\t// Keycodes")

        current_group = ""
        for code, k in keycodes.items():
            if k.get("group") != current_group:
                current_group = k.get("group")
                self.lines.append("\t/* {} */".format(current_group))
            self.lines.append("\t{:<15} = 0x{:04X},".format(k["key"], int(code, 16)))

        self.lines.append("\n\n\t// Aliases")
        for code, k in keycodes.items():
            if k["group"] != current_group:
                current_group = k["group"]
                self.lines.append("\t/* {} */".format(current_group))
            for alias in k.get("aliases", []):
                self.lines.append("\t{:<7} = {},".format(alias, k["key"]))

        self.lines.append("};\n")


    def generate_macros(self, definition):
        self.logger.debug("Generating macros")

        self.lines.append("\n// Macros")

        current_group = ""
        for m in definition.get("macros").values():
            if m.get("group") != current_group:
                current_group = m.get("group")
                self.lines.append("\n/* {} */".format(current_group))

            name = m["name"]
            if m.get("args", None): # Add "(arg1, arg2, ...)" to name if required
                name += "(" + ", ".join([arg for arg in m.get("args")]) + ")"

            if m.get("description", None):
                self.lines.append("// {}".format(m.get("description")))
            self.lines.append("#define {:<10} {}".format(name, m["define"]))

            for alias in m.get("aliases", []):
                name = m["name"]
                if m.get("args", None): # Also add args to aliases for consistency
                    arg_str = "(" + ", ".join([arg for arg in m.get("args")]) + ")"
                    alias += arg_str
                    name += arg_str
                self.lines.append("#define {:<10} {}".format(alias, name))

        self.lines.append("\n")


    def write_file(self):
        self.logger.info("Writing to file")
        out_path = "{}/{}.h".format(self.directory, self.file)
        full_path = utils.abs_pathify(out_path)
        utils.file_writer(full_path, self.lines)


    def print_file(self):
        self.logger.debug("Output file: \n\n{}\n\n".format('\n'.join(self.lines)))
