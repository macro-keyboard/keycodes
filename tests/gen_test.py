import unittest
from unittest.mock import Mock

from gen import validate_definition, range_overlapping, merge_definition
from utils import merge_dict


class TestValidateDefinition(unittest.TestCase):

    def setUp(self) -> None:
        self.logger = Mock()
        self.files = [{"name": "file1", "definition": {"ranges": {}, "keycodes": {}}},
                      {"name": "file2", "definition": {"ranges": {}, "keycodes": {}}}]
        
    def test_range_overlapping(self):
        ranges = [{"address": 0x1, "size": 0x4},
                  {"address": 0x3, "size": 0x2}]
        self.assertTrue(range_overlapping(ranges[0], ranges[1]))

        ranges = [{"address": 0x1, "size": 0x4},
                  {"address": 0x10, "size": 0x2}]
        self.assertFalse(range_overlapping(ranges[0], ranges[1]))
        
    def test_ranges_duplicate_keys(self):
        ranges1 = {"a": {"name": "a", "address": 0x100, "size": 0xFF},
                   "b": {"name": "b", "address": 0x200, "size": 0xFF}}
        ranges2 = {"a": {"name": "a", "address": 0x100, "size": 0xFF},
                   "c": {"name": "c", "address": 0x300, "size": 0xFF}}

        files = self.files.copy()
        files[0]["definition"]["ranges"] = ranges1
        files[1]["definition"]["ranges"] = ranges2
        
        with self.assertRaises(Exception):
            validate_definition(files)
            
    def test_ranges_duplicate_names(self):
        ranges1 = {"a": {"name": "a", "address": 0x100, "size": 0xFF},
                   "b": {"name": "b", "address": 0x200, "size": 0xFF}}
        ranges2 = {"c": {"name": "a", "address": 0x300, "size": 0xFF},
                   "d": {"name": "c", "address": 0x400, "size": 0xFF}}

        files = self.files.copy()
        files[0]["definition"]["ranges"] = ranges1
        files[1]["definition"]["ranges"] = ranges2
        
        with self.assertRaises(Exception):
            validate_definition(files)
            
    def test_keycodes_duplicates(self):
        keycodes1 = {"0x0001": { "group": "basic", "key": "KC_A", "label": "A"},
                     "0x0002": { "group": "basic", "key": "KC_B", "label": "B"}}
        keycodes2 = {"0x0003": { "group": "basic", "key": "KC_C", "label": "C"},
                     "0x0004": { "group": "basic", "key": "KC_B", "label": "B"}}
        
        files = self.files.copy()
        files[0]["definition"]["keycodes"] = keycodes1
        files[1]["definition"]["keycodes"] = keycodes2
        with self.assertRaises(Exception):
            validate_definition(files)
            
    def test_keycodes_duplicate_keys_and_aliases(self):
        keycodes1 = {"0x0001": { "group": "basic", "key": "KC_A", "label": "A"},
                     "0x0002": { "group": "basic", "key": "KC_B", "label": "B"}}
        keycodes2 = {"0x0003": { "group": "basic", "key": "KC_C", "label": "C"},
                     "0x0004": { "group": "basic", "key": "KC_D", "label": "D", "aliases": ["KC_A"]}}
        
        files = self.files.copy()
        files[0]["definition"]["keycodes"] = keycodes1
        files[1]["definition"]["keycodes"] = keycodes2
        with self.assertRaises(Exception):
            validate_definition(files)

    def test_keycodes_duplicate_aliases(self):
        keycodes1 = {"0x0001": { "group": "basic", "key": "KC_A", "label": "A", "aliases": ["KC_ALIAS"]},
                     "0x0002": { "group": "basic", "key": "KC_B", "label": "B"}}
        keycodes2 = {"0x0003": { "group": "basic", "key": "KC_C", "label": "C"},
                     "0x0004": { "group": "basic", "key": "KC_D", "label": "D", "aliases": ["KC_ALIAS"]}}
        
        files = self.files.copy()
        files[0]["definition"]["keycodes"] = keycodes1
        files[1]["definition"]["keycodes"] = keycodes2
        with self.assertRaises(Exception):
            validate_definition(files)
            
    def test_no_duplicates(self):
        ranges1 = {"a": {"name": "a", "address": 0x100, "size": 0xFF},
                   "b": {"name": "b", "address": 0x200, "size": 0xFF}}
        ranges2 = {"c": {"name": "c", "address": 0x300, "size": 0xFF},
                   "d": {"name": "d", "address": 0x400, "size": 0xFF}}
        keycodes1 = {"0x0001": { "group": "basic", "key": "KC_A", "label": "A"},
                     "0x0002": { "group": "basic", "key": "KC_B", "label": "B"}}
        keycodes2 = {"0x0003": { "group": "basic", "key": "KC_C", "label": "C"},
                     "0x0004": { "group": "basic", "key": "KC_D", "label": "D", "aliases": ["KC_ALIAS"]}}

        files = self.files.copy()
        files[0]["definition"]["ranges"] = ranges1
        files[1]["definition"]["ranges"] = ranges2
        files[0]["definition"]["keycodes"] = keycodes1
        files[1]["definition"]["keycodes"] = keycodes2
        self.assertTrue(validate_definition(files))

    def test_macros_duplicate_keys(self):
        macros1 = {"a": {"name": "A", "define": "A"},
                   "b": {"name": "B", "define": "B"}}
        macros2 = {"a": {"name": "A", "define": "A"},
                   "c": {"name": "C", "define": "C"}}

        files = self.files.copy()
        files[0]["definition"]["macros"] = macros1
        files[1]["definition"]["macros"] = macros2
        
        with self.assertRaises(Exception):
            validate_definition(files)

    def test_macros_duplicate_keys_and_aliases(self):
        macros1 = {"a": {"name": "A", "define": "A"},
                   "b": {"name": "B", "define": "B"}}
        macros2 = {"c": {"name": "C", "define": "C"},
                   "d": {"name": "D", "define": "D", "aliases": [ "A" ]}}
        
        files = self.files.copy()
        files[0]["definition"]["macros"] = macros1
        files[1]["definition"]["macros"] = macros2
        with self.assertRaises(Exception):
            validate_definition(files)

    def test_macros_duplicate_aliases(self):
        macros1 = {"a": {"name": "A", "define": "A", "aliases": [ "ALIAS" ]},
                   "b": {"name": "B", "define": "B"}}
        macros2 = {"c": {"name": "C", "define": "C"},
                   "d": {"name": "D", "define": "D", "aliases": [ "ALIAS" ]}}
        
        files = self.files.copy()
        files[0]["definition"]["macros"] = macros1
        files[1]["definition"]["macros"] = macros2
        with self.assertRaises(Exception):
            validate_definition(files)


class TestMergeDefinition(unittest.TestCase):

    def setUp(self) -> None:
        self.logger = Mock()
        self.files = [{"name": "file1", "definition": {"ranges": {}, "keycodes": {}}},
                      {"name": "file2", "definition": {"ranges": {}, "keycodes": {}}},
                      {"name": "file3", "definition": {"ranges": {}, "keycodes": {}}}]
    
    def test_merge_dicts(self):
        dict1 = {"a": "abc"}
        dict2 = {"b": "xyz"}

        result = merge_dict(dict1, dict2)
        self.assertEqual(result.get("a"), "abc")
        self.assertEqual(result.get("b"), "xyz")

    def test_merge_embedded_dicts(self):
        dict1 = {"a": "abc"}
        dict2 = {"b": {"c": "xyz"}}

        result = merge_dict(dict1, dict2)
        self.assertEqual(result.get("a"), "abc")
        self.assertEqual(type(result.get("b")), dict)
        self.assertEqual(result.get("b").get("c"), "xyz")
        self.assertIsNone(result.get("c"))

    def test_merge_definition(self):
        ranges1 = {"a": {"name": "a", "address": 0x100, "size": 0xFF}}
        ranges2 = {"b": {"name": "b", "address": 0x200, "size": 0xFF}}
        keycodes1 = {"0x0001": { "group": "basic", "key": "KC_A", "label": "A"}}
        keycodes2 = {"0x0002": { "group": "basic", "key": "KC_B", "label": "B"}}
        keycodes3 = {"0x0003": { "group": "basic", "key": "KC_C", "label": "C"}}
        macros1 = {"a": {"name": "A", "define": "A"}}
        macros2 = {"b": {"name": "B", "define": "B"}}

        files = self.files.copy()
        files[0]["definition"]["ranges"] = ranges1
        files[1]["definition"]["ranges"] = ranges2
        files[0]["definition"]["keycodes"] = keycodes1
        files[1]["definition"]["keycodes"] = keycodes2
        files[2]["definition"]["keycodes"] = keycodes3
        files[0]["definition"]["macro"] = macros1
        files[1]["definition"]["macro"] = macros2

        result = merge_definition(files)
        ranges = result.get("ranges")
        self.assertDictEqual(ranges.get("a"), ranges1.get("a"))
        self.assertDictEqual(ranges.get("b"), ranges2.get("b"))
        keycodes = result.get("keycodes")
        self.assertDictEqual(keycodes.get("0x0001"), keycodes1.get("0x0001"))
        self.assertDictEqual(keycodes.get("0x0002"), keycodes2.get("0x0002"))
        self.assertDictEqual(keycodes.get("0x0003"), keycodes3.get("0x0003"))
        macros = result.get("macro")
        self.assertDictEqual(macros.get("a"), macros1.get("a"))
        self.assertDictEqual(macros.get("b"), macros2.get("b"))
            



if __name__ == '__main__':
    unittest.main()